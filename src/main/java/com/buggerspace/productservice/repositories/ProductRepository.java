package com.buggerspace.productservice.repositories;


import com.buggerspace.productservice.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long> {
    Product findByName(String name);
    Product findById(long id);
    Product deleteById(String id);
}
