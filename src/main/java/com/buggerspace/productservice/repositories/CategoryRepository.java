package com.buggerspace.productservice.repositories;


import com.buggerspace.productservice.entities.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    List<Category> findAllByName(String name);
    Category findById(String id);
    Category deleteById(String id);
}
