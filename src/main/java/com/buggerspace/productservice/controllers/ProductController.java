package com.buggerspace.productservice.controllers;

import com.buggerspace.productservice.config.PriceConfigData;
import com.buggerspace.productservice.entities.Category;
import com.buggerspace.productservice.entities.Product;
import com.buggerspace.productservice.kafka.Producer;
import com.buggerspace.productservice.models.KafkaMessage;
import com.buggerspace.productservice.models.RequestObject;
import com.buggerspace.productservice.repositories.CategoryRepository;
import com.buggerspace.productservice.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/product")
public class ProductController {
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final PriceConfigData priceConfigData;

    @Value("${spring.kafka.topic.product-price-change}")
    private String productPriceChangeDestination;

    @Autowired
    KafkaTemplate<String, KafkaMessage> kafkaTemplate;
    //private final Producer producer;

    @Autowired
    public ProductController(ProductRepository productRepository, CategoryRepository categoryRepository, PriceConfigData priceConfigData) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
        this.priceConfigData = priceConfigData;
    }

    @GetMapping(path = "/getAllProducts")
    public List<Product> getAllProducts(@RequestBody RequestObject requestObject)
    {
        if(requestObject.isMobileChannel()) {
            List<Product> list = new ArrayList<>();
            for (Product product : productRepository.findAll()) {
                float webPrice = product.getPrice();
                float mobilePrice = webPrice*(100 - priceConfigData.getDiscountRate()) / 100;
                product.setPrice(mobilePrice);
                list.add(product);
            }
            return list;
        } else {
            return productRepository.findAll();
        }
    }

    @GetMapping(path = "/getProductById")
    public Object getProductById(@Valid @RequestBody RequestObject requestObject)
    {
        Product product;
        try
        {
            product = productRepository.findById(requestObject.getId());
        }
        catch (EntityNotFoundException e)
        {
            return new ResponseEntity<>("This product does not exists.", HttpStatus.NOT_FOUND);
        }
        if(requestObject.isMobileChannel()) {

            float webPrice = product.getPrice();
            float mobilePrice = webPrice*(100 - priceConfigData.getDiscountRate()) / 100;
            product.setPrice(mobilePrice);
            return product;
        } else {
            return product;
        }


    }

    @PostMapping(path = "/")
    public Object addNewProduct(@RequestBody Product product)
    {
        //Check the constraints
        if (product.getName() == null || product.getName().trim().isEmpty()) {
            return HttpStatus.BAD_REQUEST;
        }
        if (product.getImages() == null || product.getImages().size() == 0) {
            return HttpStatus.BAD_REQUEST;
        }

        HashSet<Category> categories = new HashSet<>();
        try {
            for (Category category : product.getFallIntoCategories()) {
                categories.add(categoryRepository.findById(category.getId()).orElseThrow(EntityNotFoundException::new));
            }
        }
        catch (EntityNotFoundException e) {
            return HttpStatus.BAD_REQUEST;
        }

        if (!categories.isEmpty()) {
            Product createdProduct = new Product(
                    product.getName(),
                    product.getName(),
                    product.getDescription(),
                    product.getPrice(),
                    product.getStock(),
                    product.getImages(),
                    categories);
            createdProduct = productRepository.save(createdProduct);
            System.out.println("A new Product created with id: " + createdProduct.getId() + "  and name: " + createdProduct.getName());
            return createdProduct;
        }
        else {
            return HttpStatus.BAD_REQUEST;
        }
    }

    @PutMapping(path = "/updateProduct")
    public ResponseEntity<String> updateProduct(@Valid @RequestBody Product product)
    {
        Product productEntity;
        System.out.println(product.getId() + " " + product.getName());
        try
        {
            productEntity = productRepository.getById(product.getId());
            System.out.println("The product " + productEntity.getName() + " with id " + productEntity.getId() + " is updating...");
        }
        catch (EntityNotFoundException e)
        {
            return new ResponseEntity<>("This product does not exists.", HttpStatus.NOT_FOUND);
        }

        HashSet<Category> categories = new HashSet<>();
        for (Category category : product.getFallIntoCategories()) {
            categoryRepository.findById(category.getId()).ifPresent(categories::add);
        }
        if (!categories.isEmpty()) {

            //kafka event
            float oldPrice = productEntity.getPrice();
            float newPrice = product.getPrice();
            if(oldPrice != newPrice) {

                KafkaMessage message = new KafkaMessage(product.getId(), product.getName());

                Message<KafkaMessage> kafkaMessage = MessageBuilder
                        .withPayload(message)
                        .setHeader(KafkaHeaders.TOPIC, productPriceChangeDestination)
                        .build();
                kafkaTemplate.send(kafkaMessage);
            }
            //kafka event

            productEntity.setName(product.getName());
            productEntity.setDescription(product.getDescription());
            productEntity.setPrice(product.getPrice());
            productEntity.setImages(product.getImages());
            productEntity.setFallIntoCategories(categories);
            productEntity.setStock(product.getStock());
            productRepository.save(productEntity);
            return new ResponseEntity<>("The product updated", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("The product must belongs to at least one category!", HttpStatus.BAD_REQUEST);
        }
    }


}
