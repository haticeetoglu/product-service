package com.buggerspace.productservice;

import com.buggerspace.productservice.entities.Product;
import com.github.javafaker.Book;
import com.github.javafaker.Faker;
import com.buggerspace.productservice.entities.Category;
import com.buggerspace.productservice.repositories.CategoryRepository;
import com.buggerspace.productservice.repositories.ProductRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.IntStream;

import static com.buggerspace.productservice.datagenerator.MockDataGenerator.GenerateMockProducts;

@SpringBootApplication
public class ProductServiceApplication implements CommandLineRunner {

	private final CategoryRepository categoryRepository;

	private final ProductRepository productRepository;

	public ProductServiceApplication(CategoryRepository categoryRepository, ProductRepository productRepository) {
		this.categoryRepository = categoryRepository;
		this.productRepository = productRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(ProductServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		//CreateMockData();
	}


	private void CreateMockData() {
		int mockDataSize = 100;

		Category aCategory = new Category("a");
		Category bCategory = new Category("b");
		Category cCategory = new Category("c");
		Category dCategory = new Category("d");

		aCategory = categoryRepository.save(aCategory);
		bCategory = categoryRepository.save(bCategory);
		cCategory = categoryRepository.save(cCategory);
		dCategory = categoryRepository.save(dCategory);

		List<String> imageUrls = new ArrayList<>();
		imageUrls.add("https://c.pxhere.com/photos/b1/ab/fantastic_purple_trees_beautiful_jacaranda_trees_pretoria_johannesburg_south_africa-1049314.jpg!d");
		imageUrls.add("https://c.pxhere.com/photos/90/da/jacaranda_trees_flowering_purple_stand_blossom_spring_plant-922332.jpg!d");

		Category finalACategory = aCategory;
		Category finalBCategory = bCategory;
		Category finalCCategory = cCategory;
		Category finalDCategory = dCategory;

		List<Category> categories = List.of(finalACategory, finalBCategory, finalCCategory, finalDCategory);

		ArrayList<Product> products = GenerateMockProducts(mockDataSize, categories);


		products.stream().forEach(product -> {
			productRepository.save(product);
		});




	/*	IntStream.range(1, 100).parallel().forEach(
				i -> {
					Book book = new Faker().book();
					String author = book.author();
					Product pictureProduct = new Product(author, author,
							book.title(),
							42.34f, 2, imageUrls, new HashSet<>(Arrays.asList(finalACategory, finalBCategory)));
					productRepository.save(pictureProduct);
				}
		);
	*/
	}

}
