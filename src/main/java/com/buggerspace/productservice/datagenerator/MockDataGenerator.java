package com.buggerspace.productservice.datagenerator;

import com.buggerspace.productservice.entities.Category;
import com.buggerspace.productservice.entities.Product;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.*;

public class MockDataGenerator {


    private static final Random RANDOM = new Random();

    private static final String[] WORDS = new String[] {
            "Lorem","ipsum", "dolor", "sit", "amet", "consectetur",
            "adipiscing", "elit", "vivamus", "accumsan", "commodo",
            "magna", "morbi", "eget", "porttitor", "elit", "sed",
            "rhoncus", "nisl", "maecenas", "fringilla", "elit", "non"
    };

    private static final float[] FLOAT_NUMBERS = new float[] {
            43.34f, 53.0f, 123.94f, 21f, 34.2f
    };

    private static final int[] INTEGER_NUMBERS = new int[] {
            45, 543, 32, 90, 23, 19
    };

    private static final String[] IMAGE_URLS = new String[] {
            "https://c.pxhere.com/photos/b1/ab/fantastic_purple_trees_beautiful_jacaranda_trees_pretoria_johannesburg_south_africa-1049314.jpg!d",
            "https://c.pxhere.com/photos/90/da/jacaranda_trees_flowering_purple_stand_blossom_spring_plant-922332.jpg!d"
    };



     public static ArrayList<Product> GenerateMockProducts(int number, List<Category> categories) {
        ArrayList<Product> productList = new ArrayList<Product>();

        int wordListSize = WORDS.length;
        int floatListSize = FLOAT_NUMBERS.length;
        int intListSize = INTEGER_NUMBERS.length;
        int imageListSize = IMAGE_URLS.length;
        int categoryListSize = categories.size();

        for (int createdProductNumber = 0; createdProductNumber < number; createdProductNumber++) {
            String name = WORDS[RANDOM.nextInt(wordListSize)];
            String name2 = WORDS[RANDOM.nextInt(wordListSize)];
            String description = WORDS[RANDOM.nextInt(wordListSize)];
            float price = FLOAT_NUMBERS[RANDOM.nextInt(floatListSize)];
            int stock = INTEGER_NUMBERS[RANDOM.nextInt(intListSize)];

            String imageUrl = IMAGE_URLS[RANDOM.nextInt(imageListSize)];
            List<String> imageUrls = new ArrayList<>();
            imageUrls.add(imageUrl);

            Category category = categories.get(RANDOM.nextInt(categoryListSize));
            HashSet<Category> categoryHashSet = new HashSet<>(Arrays.asList(category));


            Product product = new Product(name, name2, description, price, stock, imageUrls, categoryHashSet);
            productList.add(product);
        }
        return productList;
    }
}
