package com.buggerspace.productservice.models;

public class RequestObject {

    private ChannelType channelType;

    private long id;

    public RequestObject() {}

    public RequestObject(ChannelType channelType, long id) {
        this.channelType = channelType;
        this.id = id;
    }

    public RequestObject(ChannelType channelType) {
        this.channelType = channelType;
    }

    public boolean isMobileChannel() {
        if(this.channelType == ChannelType.MOBILE) {
            return true;
        }
        return false;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType channelType) {
        this.channelType = channelType;
    }

    public long getId() {
        return id;
    }
}
