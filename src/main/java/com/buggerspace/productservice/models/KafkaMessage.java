package com.buggerspace.productservice.models;

public class KafkaMessage {
    long productId;
    String productName;

    public KafkaMessage(long productId, String productName) {
        this.productId = productId;
        this.productName = productName;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public String toString() {
        return "KafkaMessage{" +
                "productId=" + productId +
                ", productName='" + productName + '\'' +
                '}';
    }


}
