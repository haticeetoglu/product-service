package com.buggerspace.productservice.entities;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "products",indexes = @Index(columnList = "name"))
public class Product
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String name2;
    private String description;
    @NotNull
    private float price;

    private Integer stock;

    @ElementCollection
    @CollectionTable(name = "Product_Images", joinColumns = @JoinColumn(name = "product_id", nullable = false))
    @Column(name = "image_URL", nullable = false)
    @Size(min = 1)
    @NotNull
    private List<String> images;


    @ManyToMany
    @JoinTable(name = "product_category",
            joinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id"))
    @Size(min = 1)
    @NotNull
    private Set<Category> fallIntoCategories;

    public Product() {}

    public Product(Product product) {
        this.name = product.getName();
        this.name2 = product.getName2();
        this.description = product.getDescription();
        this.price = product.getPrice();
        this.stock = product.getStock();
        this.images = product.getImages();
        this.fallIntoCategories = product.getFallIntoCategories();
    }

    public Product(String name,
                   String name2,
                   String description,
                   float price,
                   Integer stock,
                   List<String> images,
                   Set<Category> fallIntoCategories) {
        this.name = name;
        this.name2 = name2;
        this.description = description;
        this.price = price;
        this.stock = stock;
        this.images = images;
        this.fallIntoCategories = fallIntoCategories;
    }



    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }


    public String getName2()
    {
        return name2;
    }

    public void setName2(String name2)
    {
        this.name2 = name2;
    }


    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public float getPrice()
    {
        return price;
    }

    public void setPrice(float price)
    {
        this.price = price;
    }

    public List<String> getImages()
    {
        return images;
    }

    public void setImages(List<String> images)
    {
        this.images = images;
    }

    public Set<Category> getFallIntoCategories()
    {
        return fallIntoCategories;
    }

    public void setFallIntoCategories(HashSet<Category> fallIntoCategories)
    {
        this.fallIntoCategories = fallIntoCategories;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }
}

